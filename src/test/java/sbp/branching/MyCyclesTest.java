package sbp.branching;


import org.junit.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.mockito.ArgumentMatchers.anyString;

public class MyCyclesTest
{
    Utils utilsMock;
    MyCycles myCycles;
    /**
     * Проверка количества обращений к {@link Utils#utilFunc1} в {@link MyCycles#cycleForExample}
     * Метод Utils#utilFunc1() вызван iterations раз
     */
    @Test
    public void cycleForExample_Test()
    {
        final int iterations = 2;
        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);
        myCycles = new MyCycles(utilsMock);
        myCycles.cycleForExample(iterations, "Hello!");
        Mockito.verify(utilsMock, Mockito.times(iterations)).utilFunc1(anyString());
    }

    /**
     * Проверка количества обращений к {@link Utils#utilFunc1} в {@link MyCycles#cycleWhileExample}
     * Метод Utils#utilFunc1() вызван iterations раз, когда Utils#utilFunc1() возвращает ture
     * Метод Utils#utilFunc1() вызван 1 раз, когда Utils#utilFunc1() возвращает false
     */
    @Test
    public void cycleWhileExample_Test()
    {
        final int iterations = 3;

        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);
        myCycles = new MyCycles(utilsMock);
        myCycles.cycleWhileExample(iterations, "Hello!");
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());

        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);
        myCycles = new MyCycles(utilsMock);
        myCycles.cycleWhileExample(iterations, "Hello!");
        Mockito.verify(utilsMock, Mockito.times(iterations)).utilFunc1(anyString());
    }


    /**
     * Проверка количества обращений к {@link Utils#utilFunc1} в {@link MyCycles#cycleDoWhileExample}
     * Метод Utils#utilFunc1() вызван 1 раз, когда Utils#utilFunc1() возвращает false, не зависимо от значений from и to
     * Метод Utils#utilFunc1() вызван to - from раз, когда Utils#utilFunc1() возвращает true
     * Метод Utils#utilFunc1() вызван 1 раз, когда Utils#utilFunc1() возвращает true и аргументы form и to равны
     */
    @Test
    public void cycleDoWhileExample_Test()
    {
        int from = 2;
        int to = 3;
        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);
        myCycles = new MyCycles(utilsMock);
        myCycles.cycleDoWhileExample(from, to, "Hello!");
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());

        from = 4;
        to = 4;
        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);
        myCycles = new MyCycles(utilsMock);
        myCycles.cycleDoWhileExample(from, to, "Hello!");
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());

        from = 2;
        to = 4;
        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);
        myCycles = new MyCycles(utilsMock);
        myCycles.cycleDoWhileExample(from, to, "Hello!");
        Mockito.verify(utilsMock, Mockito.times(to - from)).utilFunc1(anyString());

        from = 2;
        to = 2;
        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);
        myCycles = new MyCycles(utilsMock);
        myCycles.cycleDoWhileExample(from, to, "Hello!");
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
    }

}
