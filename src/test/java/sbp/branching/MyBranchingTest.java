package sbp.branching;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

public class MyBranchingTest {
    Utils utilsMock;
    MyBranching myBranching;

    /**
     * Проверка возвращаемого значения от {@link MyBranching#ifElseExample}
     * возвращает true, при Utils#utilFunc2 возвращаемом true
     */
    @Test
    public void ifElseExamplePositive_Test() {
        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        myBranching = new MyBranching(utilsMock);
        Assert.assertEquals(true, myBranching.ifElseExample());
    }

    /**
     * Проверка возвращаемого значения от {@link MyBranching#ifElseExample}
     * возвращает false, при Utils#utilFunc2 возвращаемом false
     */
    @Test
    public void ifElseExampleNegative_Test() {
        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        myBranching = new MyBranching(utilsMock);
        Assert.assertEquals(false, myBranching.ifElseExample());
    }

    /**
     * Проверка количества обращений к {@link MyBranching#switchExample}
     * при входящем i != 1 и i != 2 и Utils#utilFunc2 возвращающем ture, Utils#utilFunc1 выполнится 1 раз, Utils#utilFunc2 выполнится 1 раз
     */
    @Test
    public void switchExampleInZero_Test() {
        int i = 0;
        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();

    }

    /**
     * Проверка количества обращений к {@link MyBranching#switchExample}
     * при входящем i != 1 и i != 2 и Utils#utilFunc2 возвращающем false, Utils#utilFunc1 выполнится 0 раз, Utils#utilFunc2 выполнится 1 раз
     */
    @Test
    public void switchExampleInZero2_Test() {
        int i = 0;
        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();

    }

    /**
     * Проверка количества обращений к {@link MyBranching#switchExample}
     * при входящем i = 1, Utils#utilFunc1 выполнится 1 раз, Utils#utilFunc2 выполнится 1 раз
     */
    @Test
    public void switchExampleInOne_Test() {
        int i = 1;
        utilsMock = Mockito.mock(Utils.class);
        myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();

    }

    /**
     * Проверка количества обращений к {@link MyBranching#switchExample}
     * при входящем i = 2, Utils#utilFunc1 выполнится 0 раз, Utils#utilFunc2 выполнится 1 раз
     */
    @Test
    public void switchExampleInTwo_Test() {
        int i = 2;
        utilsMock = Mockito.mock(Utils.class);
        myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    /**
     * Проверка возвращаемого значения от {@link MyBranching#maxInt}
     * возвращает большее из значений, если Utils#utilFunc2 вернёт false
     */
    @Test
    public void maxIntReturnsMaximum_Test() {
        int i1 = 2;
        int i2 = 4;
        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        myBranching = new MyBranching(utilsMock);
        Assert.assertEquals(i1 >= i2 ? i1 : i2, myBranching.maxInt(i1, i2));
        Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1(anyString());

        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        myBranching = new MyBranching(utilsMock);
        Assert.assertEquals(i1 >= i2 ? i1 : i2, myBranching.maxInt(i1, i2));
        Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    /**
     * Проверка возвращаемого значения от {@link MyBranching#maxInt}
     * возвращает 0, если Utils#utilFunc2 вернёт true
     */
    @Test
    public void maxIntReturnsZero_Test() {
        int i1 = 2;
        int i2 = 4;

        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        myBranching = new MyBranching(utilsMock);
        Assert.assertEquals(0, myBranching.maxInt(i1, i2));
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());

        utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        myBranching = new MyBranching(utilsMock);
        Assert.assertEquals(0, myBranching.maxInt(i1, i2));
    }
}
